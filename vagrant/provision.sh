# The output of all these installation steps is noisy. With this utility
# the progress report is nice and concise.
function install {
    echo installing $1
    shift
    apt-get -y install "$@" >/dev/null 2>&1
}

echo adding swap file
fallocate -l 2G /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo '/swapfile none swap defaults 0 0' >> /etc/fstab

install 'development tools' build-essential
install 'autossh' autossh
install 'screen' screen

# Needed for docs generation.
update-locale LANG=en_US.UTF-8 LANGUAGE=en_US.UTF-8 LC_ALL=en_US.UTF-8

# update apt-repository
apt-get update

# install Git
install Git git

#install MavProxy
apt-get -y install python-dev python-opencv python-matplotlib python-pygame python-lxml

wget https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
pip install MAVProxy
pip install dronekit
pip install dronekit-sitl

echo "export PATH=$PATH:$HOME/.local/bin" >> ~/.bashrc
