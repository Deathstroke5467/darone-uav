Darone UAV
======


## Get connected with Darone.io

[How To Use](#how-to-use)

[Installation Guide](#install-from-bitbucket)

[Disclaimer](#Disclaimer)

How To Use
------


### Don't have a Raspberry Pi or UAV?

install Vagrant on your computer and follow instructions below

~~~~
git clone git@bitbucket.org:daroneio/darone-uav.git
cd darone-uav
vagrant up
vagrant ssh
cd /vagrant
~~~~

### Make sure to replace the values below

/usr/share/darone/conf/darone.conf

```
AUTH_TOKEN=<auth_token>
EMAIL=your@email.com
```

### Test connectivity
`darone ping`

### Start/Stop darone service with

```
darone start
darone stop
```

### Congrats! Your UAV is now added to darone.io

Install from Bitbucket
------


### Clone this git repository on your Raspberry Pi
```
git clone git@bitbucket.org:daroneio/darone-uav.git
cd darone-uav
vi darone.sh
```

### Install Program & Generate ssh-keys
`./install`

Disclaimer
------


This program is often referred to as darone.sh and/or darone, not to be confused 
with darone.io and its web user interface, which are seperate entities.


You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Copyright (C) 2017  Johan Sanden and Marcello Barbieri

Please address any additional questions [here](https://bitbucket.org/daroneio/darone-uav/src).