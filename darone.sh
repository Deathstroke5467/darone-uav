#!/bin/bash

## file darone.sh
## Author Johan Sanden, Marcello Barbieri
## https://darone.io ###

#sets directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#Source Files
. $DIR/conf/darone.cfg
. $DIR/lib/darone-lib.cfg


INTERFACE=$(ip r | grep default | head -1 | awk '{print $5}')
UUID=$(/sbin/blkid | grep -oP 'UUID="\K[^"]+' | sha256sum | awk '{print $1}')
LOCAL_IP=$(ip -4 a l $INTERFACE | grep -Po 'inet \K[\d.]+')
DARONE_CLOUD_URL="https://darone.io"

#[ ! -f darone.cfg ] && echo "Please run './darone.sh install' first" && exit 1

check_ssh_keys() {
	## initial checks
	if [ ! -f $HOME/.ssh/id_rsa.pub ]; then
    printmenu
		printf '%s\n' "Please run '$0 install' to generate new SSH keys"
		exit 1;
	else
		PUBKEY=$(cat $HOME/.ssh/id_rsa.pub|cut -f2 -d" ")
	fi
}

ping_server() {
  printmenu
	printf '%s\n' "Testing connectivity"
	IP=$(echo $DARONE_CLOUD_URL|cut -f3 -d"/"|cut -f1 -d":")
	ping -w 2 -c 1 $IP >/dev/null 2>&1 #put this directly in if statement

	if [ $? == 0 ]; then #optimize if statement
		echo -e "[ \e[32m OK \e[0m ] - ping $IP"
		 curl -I $DARONE_CLOUD_URL >/dev/null 2>&1
		 if [ $? == 0 ]; then #put this directly in if statement
			 printf '%s\n\n' "[ ${GREEN} OK ${NORMAL} ] - curl $DARONE_CLOUD_URL"
		 else
			 printf '%s\n\n' "[ ${RED} OFFLINE ${NORMAL} ] - $DARONE_CLOUD_URL"
		 fi
	else
		printf '%s\n\n' "[ ${RED} OFFLINE ${NORMAL} ] - $IP"
	fi
}

### stream video to darone
start_video() {
  printmenu
	PID=$(ps -ef | grep video-stream.sh|grep -v grep|awk '{print $2}'|head -1)

	if [ -z $PID ] ; then
		printf '%s\n' "Starting videostream..."
		nohup $DIR/scripts/video-stream.sh $1 > $DIR/logs/video-stream.log 2>&1 &
	else
		printf '%s\n' "Already started with pid $PID"
	fi
}

start_tunnel() {
  printmenu " Darone - Starting tunnel for port $1"

  PID=$(ps aux|grep "[t]unnel"|grep $1|awk '{print $2}')
	if [ -z $PID ]; then
		printf '%s\n' "start tunnel for 127.0.0.1:$2 => $DARONE_ASSIGNED_TUNNEL_SERVER:$1"
		nohup $DIR/scripts/tunnel.sh $1 $2 $SSH_USER $DARONE_ASSIGNED_TUNNEL_SERVER $BIND_EXTERNAL_TO > $DIR/logs/tunnel.log 2>&1 &
	else
		printf '%s\n' "Already running with PID $PID"
	fi
}

start_tunnels() {
	printmenu " Darone - Starting all tunnels"
	ports="$LOCAL_MAVLINK_PORT $LOCAL_DCONTROL_PORT $LOCAL_SSH_PORT"
	for port in $ports ; do
		if [ ! -z $port ]; then
			start_tunnel $DARONE_ASSIGNED_PORT $port
		fi
	done
}

stop_tunnels() {
	printmenu " Darone - Stopping all tunnels"
	ports="$LOCAL_MAVLINK_PORT $LOCAL_DCONTROL_PORT $LOCAL_SSH_PORT"
	for port in $ports ; do
		echo "closing tunnel $port"
		PID=$(ps wup $(pgrep -f tunnel) |grep $port|awk '{print $2}')
			if [ ! -z $port ] && [ ! -z $PID ] ; then
				kill $(pgrep -P $PID)
				kill $PID
			fi
	done
}


tunnel_status() { #make this more dynamic?

  #pgrep -P $(ps aux|grep "[t]unnel"|grep $LOCAL_MAVLINK_PORT|awk '{print $2}')


	TUNNEL_STATUS_MAVLINK=$(ps -ef|grep "$DARONE_ASSIGNED_PORT:0.0.0.0:$LOCAL_MAVLINK_PORT"|grep -v grep|awk '{print $2}')
	if [ ! -z $TUNNEL_STATUS_MAVLINK ] ; then
		TUNNEL_STATUS_MAVLINK="\e[32mUP (ONLINE) "
	else
		TUNNEL_STATUS_MAVLINK="\e[5m\e[31mDOWN"
	fi

	TUNNEL_STATUS_DCONTROL=$(ps -ef|grep "$DARONE_ASSIGNED_DPORT:0.0.0.0:$LOCAL_DCONTROL_PORT"|grep -v grep|awk '{print $2}')
	if [ ! -z $TUNNEL_STATUS_DCONTROL ] ; then
		TUNNEL_STATUS_DCONTROL="\e[32mUP (ONLINE) "
	else
		TUNNEL_STATUS_DCONTROL="\e[5m\e[31mDOWN"
	fi

	TUNNEL_STATUS_SSH=$(ps -ef|grep "$DARONE_ASSIGNED_SSHPORT:0.0.0.0:$LOCAL_SSH_PORT"|grep -v grep|awk '{print $2}')
	if [ ! -z $TUNNEL_STATUS_SSH ] ; then
		TUNNEL_STATUS_SSH="\e[32mUP (ONLINE) "
	else
		TUNNEL_STATUS_SSH="\e[5m\e[31mDOWN"
	fi
}

### stop dcontrol daemon
start_dcontrol() {
  printmenu "Darone - Start dcontrol"

  PID=$(pgrep -x dcontrol)
	if [ -z $PID ] ; then
		printf '%s\n' "Starting dcontrol..."
		nohup $DIR/dcontrol > $DIR/logs/dcontrol.log 2>&1 &
	else
		printf '%s\n' "Already started dcontrol with pid $PID"
	fi
}

start_simulator() {
  printmenu " Darone - Start simulator"

	PID=$(pgrep -x simulator.sh)
	if [ -z $PID ] ; then
		printf '%s\n' "[ ${GREEN} OK ${NORMAL} ] - Starting simulator"
		nohup $DIR/scripts/simulator.sh > $DIR/logs/simulator.log 2>&1 &
	else
		printf '%s\n' "[ ${GREEN} OK ${NORMAL} ] - Already started with pid $PID"
	fi
}

start_arducopter_quad() {
  printmenu " Darone - Starting ArduCopter-Quad"

	PID=$(pgrep -x arducopter_quad.sh)
	if [ -z $PID ] ; then
		printf '%s\n' "[ ${GREEN} OK ${NORMAL} ] - Starting ArduCopter"
		nohup $DIR/scripts/arducopter_quad.sh > $DIR/logs/arducopter_quad.log 2>&1 &
	else
		printf '%s\n' "[ ${GREEN} OK ${NORMAL} ] - Already started with pid $PID"
	fi
}

stopd() {
	opt=$1
	for pName in $2 ; do
    PID=$(pgrep -x $pName) #set parent id
		if [ ! -z $PID ] ; then #if process name exists # [! -z $PID] || printf '%s\n' "[ ${GREEN} OK ${NORMAL} ] - No $pName to stop" && break
		  case $opt in
		    g)
					CPID=$(pgrep -P $PID) #set childs
					kill $(pgrep -P $CPID)  ;& #kill grandchilds
		    c)
		    	kill -9 $(pgrep -P $PID) ;& #kill childs
		    p)
					kill -9 $PID # kill parent
					printf '%s\n' "[ ${GREEN} OK ${NORMAL} ] - $pName stopped with parent id $PID" ;;
		  esac
		else
			printf '%s\n' "[ ${GREEN} OK ${NORMAL} ] - No $pName to stop"
		fi
	done
}

stop() { #kill parent processes

  for pName in $1 ; do #set $programs to use $1
    PID=$(pgrep -x $pName) #set parent id
    if [ ! -z $PID ] ; then #if process name exists
      printf '%s\n' "Stopping $pName"
  		kill -9 $PID # kill parent
      printf '%s\n' "[ ${GREEN} OK ${NORMAL} ] - $pName stopped with parent id $PID"
    else
      printf '%s\n' "[ ${GREEN} OK ${NORMAL} ] - No $pName to stop"
    fi
  done
}

stopc() { #kill parent and child processes
  for pName in $1 ; do
    PID=$(pgrep -x $pName) #set parent id
    if [ ! -z $PID ] ; then #if process name exists
      printf '%s\n' "Stopping $pName"
      kill -9 $(pgrep -P $PID) #kill childs
      kill -9 $PID #kill parent
      printf '%s\n' "[ ${GREEN} OK ${NORMAL} ] - $pName stopped with parent id $PID"
    else
      printf '%s\n' "[ ${GREEN} OK ${NORMAL} ] - No $pName to stop"
    fi
  done
}

stopcc() { #kill parent, child, and grandchild processes #do cascading functions?
  for pName in $1 ; do
    PID=$(pgrep -x $pName)
    if [ ! -z $PID ] ; then #if process name exists
      printf '%s\n' "Stopping $pName"
      CPID=$(pgrep -P $PID) #set childs
      kill $(pgrep -P $CPID) #kill grandchilds
      kill -9 $CPID #kill childs
      kill -9 $PID #kill parent
      printf '%s\n' "[ ${GREEN} OK ${NORMAL} ] - $pName stopped with parent id $PID"
    else
      printf '%s\n' "[ ${GREEN} OK ${NORMAL} ] - No $pName to stop"
    fi
  done
}

stopall() { #rename to stop main?
	printmenu
  stop dcontrol
  stop_tunnels
	stopcc simulator.sh
}

reboot() { #rethink this command, needed? should it kill arducopter then?
		stopall
		sudo reboot
}

shutdown() { #rethink this command, needed? should it kill arducopter then?
		stopall
		sudo shutdown -h now
}

start() {

	[ -z "$AUTH_TOKEN" ] && echo "AUTH_TOKEN missing, please update darone.cfg file" && exit 1
	[ -z "$EMAIL" ] && echo "EMAIL missing, please update darone.cfg file" && exit 1
	[ -z "$UAV_NAME" ] && echo "UAV_NAME missing, please update darone.cfg file" && exit 1

	EXTERNAL_IP=""

	while [ -z "$EXTERNAL_IP" ]; do
		 EXTERNAL_IP=$(curl -s http://ipinfo.io|grep ip|sed -e 's/[A-Za-z]*//g' -e 's/ //g' -e 's/\"//g' -e 's/\://g' -e 's/\,//g')
		 echo "Discovering external IP...."
	   sleep 5
	done

  #check that we have ssh keys generated
	check_ssh_keys

  #stop any running tunnels
  stop_tunnels

  #start dcontrol binary
	start_dcontrol

	### get settings from UAV regserver via POST
	curl -s -H "Content-Type: application/json" -X POST -d '{"auth_token":"'$AUTH_TOKEN'","ip":"'$EXTERNAL_IP'","name":"'$UAV_NAME'","email":"'$EMAIL'", "uuid":"'$UUID'" ,"pubkey":"'$PUBKEY'"}' $DARONE_CLOUD_URL/api/uav_register|json_pp > $DIR/logs/uav.json

	SSH_USER=$(cat $DIR/logs/uav.json|grep ssh_user|head -1|cut -f4 -d"\"")
	DARONE_ASSIGNED_PORT=$(cat $DIR/logs/uav.json|grep mavlink_port|head -1|sed 's/[^0-9]*//g')
	DARONE_ASSIGNED_DPORT=$(cat $DIR/logs/uav.json|grep dcontrol_port|head -1|sed 's/[^0-9]*//g')
	DARONE_ASSIGNED_SSHPORT=$(cat $DIR/logs/uav.json|grep ssh_port|head -1|sed 's/[^0-9]*//g')
	DARONE_ASSIGNED_TUNNEL_SERVER=$(cat $DIR/logs/uav.json| grep ip| sed 's/[^0-9.]*//g')

	### send settings
  printmenu "Add keys to darone node server $DARONE_ASSIGNED_TUNNEL_SERVER"
	curl -s -H "Content-Type: application/json" -X POST -d '{"auth_token":"'$AUTH_TOKEN'","ip":"'$EXTERNAL_IP'","name":"'$UAV_NAME'","email":"'$EMAIL'", "uuid":"'$UUID'" ,"pubkey":"'$PUBKEY'"}' $DARONE_ASSIGNED_TUNNEL_SERVER:3001/add_keys|json_pp > $DIR/logs/pubkeys.json

	## start tunnel for both mavlink and dcontrol
	##
	if [ -z "$EXTERNAL_PORTS" ] || [ "$EXTERNAL_PORTS" == "no" ] ; then
		BIND_EXTERNAL_TO="127.0.0.1"
	elif [ "$EXTERNAL_PORTS" == "yes" ] ; then
		BIND_EXTERNAL_TO='0.0.0.0'
	fi

  #start the tunnels
  start_tunnels

	printf '%s\n' "Sleeping for 3 seconds until the tunnels are up"
	sleep 3

  #check tunnel statuses
	tunnel_status

	printf '%s\n' "-----------------------------------------"
	printf '%s\n' "Updates your uav info to darone server"
	printf '%s\n' "-----------------------------------------"
	printf '%s\n' "Darone.io mail: $EMAIL"
	printf '%s\n' "Darone.io auth_token: $AUTH_TOKEN"
	printf '%s\n' "-----------------------------------------"
	printf '%s\n' "UAV UUID: $UUID"
	printf '%s\n' "UAV IP: $LOCAL_IP"
	printf '%s\n' "UAV Internet: $EXTERNAL_IP"
	printf '%s\n' "UAV Mavlink Port: $LOCAL_MAVLINK_PORT"
  printf '%s\n' "UAV Assigned Node: $DARONE_ASSIGNED_TUNNEL_SERVER"
	printf '%s\n' "----------------------------------------"
	echo -e "Tunnel Status Mavlink: $TUNNEL_STATUS_MAVLINK \e[0m"
	echo -e "Tunnel Status Dcontrol: $TUNNEL_STATUS_DCONTROL \e[0m"
	echo -e "Tunnel Status SSH Access: $TUNNEL_STATUS_SSH \e[0m"
	printf '%s\n' "------------------------------"
}

boot_set () {

  printf '\n%s' "Current Boot Status:"
  if systemctl is-enabled darone.service ; then
    printf '%s\n'  "[  ${GREEN}ON${NORMAL}  ]" "Script will start at boot\n" ""
  else
    printf '%s\n' "[  ${RED}OFF${NORMAL}  ]" "Script will not start at boot" ""
  fi

  printf '%s\n' "Press Enter to exit" "Press 1 to start at boot" "Press 0 to not start at boot"
  read input
  case $input in
    0)
      printf '%s\n' "script will not start at boot"
      systemctl enable darone.service && exit ;;
    1)
      printf '%s\n' "script will now start at boot"
      systemctl disable darone.service && exit;;
    *)
      exit ;;
  esac
}


case "$1" in
  ping)
		ping_server
    ;;
  start)
		start
    ;;
  stop)
    stopall
    ;;
  connect)
    connect
    ;;
  install)
    ./install.sh
    ;;
  restart)
    start
    ;;
  start_arducopter_quad)
    start_arducopter_quad
    ;;
  stop_arducopter_quad)
		printmenu
    stopcc arducopter_quad.sh
    ;;
  start_simulator)
    start_simulator
    ;;
  stop_simulator)
		printmenu
    stopcc simulator.sh
    ;;
  start_video_low)
    start_video low
    ;;
  start_video_medium)
    start_video medium
    ;;
  start_video_high)
    start_video high
    ;;
  stop_video)
		printmenu
    stop video-stream.sh
    ;;
  start_dcontrol)
    start_dcontrol
    ;;
  stop_dcontrol)
		printmenu
    stop dcontrol
    ;;
  reboot)
    reboot
    ;;
  shutdown)
    shutdown
    ;;
  boot)
    boot_set
    ;;
  *)
		printf '%s\n' "" "NAME:"    " darone.sh - Darone.io Cloud Control Platform for UAVs (${BLUE}${UNDERLINE}https://darone.io${NORMAL})"
		printf '%s\n' "" "VERSION:" "    ${BLUE}0.0.1${NORMAL}"
		printf '%s\n' "" "AUTHOR:"  "    Darone - support@darone.io"
		printf '\n%s\n' "COMMANDS:"
		printf '%s\n' "    ping			Test connectivity to Darone webservice"
		printf '%s\n' "    install			Updates scripts, ssh keys and recompiles dcommand binary"
		printf '%s\n' "    start			Establish tunnels to Darone.io for MAVLink and Dcommand"
		printf '%s\n' "    stop			Stop all related Darone processes"
		printf '%s\n' "    restart			Stop and start Darone processes"
    printf '%s\n' "    boot   			Check and set if script starts at boot"
		printf '%s\n' "    start_arducopter_quad	Starting ArduCopter Quad using script"
		printf '%s\n' "    stop_arducopter_quad	Stopping ArduCopter"
		printf '%s\n' "    start_simulator		Starting simulator using script"
		printf '%s\n' "    stop_simulator		Stopping simulator background process"
		printf '%s\n' "    start_video_high		Start videostream with high quality"
		printf '%s\n' "    start_video_medium		Start videostream with medium quality"
		printf '%s\n' "    start_video_low		Start videostream with low quality"
		printf '%s\n' "    stop_video			Stop any video stream"
		printf '%s\n' "    start_dcontrol		Start dcontrol daemon"
		printf '%s\n' "    stop_dcontrol		Stop dcontrol daemon"
		printf '%s\n' "    reboot			Reboot the RPi"
		printf '%s\n' "    shutdown			Shutdown the RPi"
		printf '\n%s\n\n' " Usage: $0 ping" ;;
esac
