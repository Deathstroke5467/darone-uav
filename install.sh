#!/bin/bash

#use file compression?

##COLOR LIB##
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
BLUE=$(tput setaf 4)
NORMAL=$(tput sgr0)

DIR=/usr/share/darone

#Creates Dcontrol
printf '%s\n' "----------------------------------------" " Darone - Building D-control Binary  	  " "----------------------------------------"

[ -f dcontrol ] && rm dcontrol
gcc -pthread -o dcontrol src/dcontrol.c

if [ -f dcontrol ] ; then
	printf '%s\n' "[ ${GREEN} OK ${NORMAL} ] - ./dcontrol binary created"
fi

#create directories
sudo install -d -m 755 $DIR/scripts
sudo install -d -m 755 $DIR/lib
sudo install -d -m 777 $DIR/logs
#install function libraries
sudo install -m 644 lib/* $DIR/lib/
#install configuration Files
sudo install -m 644 conf/* $DIR/conf/
#install darone & dcontrol
sudo install -m 755 darone.sh $DIR/
sudo install -m 755 dcontrol $DIR/
#install all other scripts
sudo install -m 755 scripts/* $DIR/scripts/

#Creates service file to start at boot
printf '%s\n' "----------------------------------------" " Darone - Generating Service File" "----------------------------------------"

#add sleep on reload?
sudo cat <<'EOF'> /etc/systemd/system/darone.service
[Unit]
Description=manages darone.sh

[Service]
Type=oneshot
ExecStart=$DIR/darone.sh start
ExecStop=$DIR/darone.sh stop
ExecReload=$DIR/darone.sh stop ; sleep 5 ; $DIR/darone.sh start

[Install]
WantedBy=multi-user.target

EOF



printf '%s\n' "----------------------------------------" " Darone - Creating Alias" "----------------------------------------"

cd $HOME #make dynamic
[ ! -f .bash_aliases ] && sudo touch .bash_aliases
printf '%s\n' "alias darone='$DIR/darone.sh'" >> .bash_aliases
. ~/.bash_aliases

if ! type "jo" &> /dev/null; then
  # install foobar here
printf '%s\n' "-----------" " Adding jo " "-----------"
sudo apt-get install -y autoconf automake 
git clone git://github.com/jpmens/jo.git
cd jo
printf '%s\n' "Configuring jo"
autoreconf -i
./configure
make check
printf '%s\n' "Installing jo"
make install
else 
printf '%s\n' "jo already installed"
fi


setup() {
#Creates SSH Keys
printf '%s\n' "----------------------------------------" " Darone - Generating SSH keys  	        " "----------------------------------------"

if [ ! -f ~/.ssh/id_rsa.pub ] ; then
	printf '%s\n' "No keys found generate a new pair, use default values"
	printf '%s\n' "and with no passphrase, file (~/.ssh/id_rsa.pub)"
	ssh-keygen
else
	printf '%s\n' "[ ${GREEN} OK ${NORMAL} ] - Keys found ~/.ssh/id_rsa.pub."
	printf '%s\n' "[ ${BLUE} INFO ${NORMAL} ] - Remove old keys if you would like a new pair."
fi
}

setup #generates new key

printf '%s\n' \
"############################################" \
"Darone Script" "Created By Johan Sanden & Marcello Barbieri" "${BLUE}https://darone.io${NORMAL}" \
"############################################"
