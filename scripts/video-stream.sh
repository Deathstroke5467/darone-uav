#!/bin/bash
# quality not implemented yet
if [ $# -eq 0 ]
  then
    QUALITY="low"
fi

case "$1" in
 "h" | "high" | "1080")
   echo "Using FULL HD stream quality"
   frame_rate=30
   width=1920
   height=1080
   initbr=7776000
   avgbr=7906000
   peakbr=8006000
   minbr=7906000
  ;;
 "m" | "medium" | "720")
   echo "Using HD stream quality"
   frame_rate=30
   width=1280
   height=720
   initbr=3500000
   avgbr=3586000
   peakbr=4000000
   minbr=3500000

  ;;
 "l" | "low" | "360")
   echo "Using low stream quality"
   frame_rate=30
   width=640
   height=360
   initbr=1000000
   avgbr=1000000
   peakbr=1200000
   minbr=850000
  ;;

   * )
   echo "Using default low stream quality"
   frame_rate=30
   width=640
   height=360
   initbr=1000000
   avgbr=1000000
   peakbr=1200000
   minbr=850000
  ;;
esac

## other options
## C920 camera
### this uses onboard h264 chip in raspberry for low CPU
# Set power line frequency to 1: 50 Hz (default 2: 60 Hz -> flicker)
v4l2-ctl --set-ctrl=power_line_frequency=1

## this is used now to launch GStreamer
gst-launch-1.0 -vvv uvch264src initial-bitrate=2500000 \
      average-bitrate=2200000 peak-bitrate=2500000 \
      minimum-bitrate=2000000 fixed-framerate=true \
      iframe-period=2000 device=/dev/video0 name=src \
      auto-start=true do-timestamp=true src.vidsrc  ! video/x-h264,width=$width,height=$height,profile=main,framerate=$frame_rate/1 ! h264parse ! rtph264pay config-interval=10 pt=96 ! udpsink host=139.59.128.165 port=8004
