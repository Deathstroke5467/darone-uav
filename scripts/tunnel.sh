#!/bin/bash
while true; do
	 echo "Trying to connect to $4 ..."
	 ssh -o UserKnownHostsFile=/dev/null -o ExitOnForwardFailure=yes -o StrictHostKeyChecking=no -o ServerAliveInterval=240 -R $5:$1:0.0.0.0:$2 -N $3@$4
	 echo "restarting in 5 seconds.."
	 sleep 5
done
