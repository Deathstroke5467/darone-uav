#!/bin/bash


#Grab Cameras
#v4l2-ctl --list-devices
#or
if ls -ltrh /dev/video* ; then
  dev=$(jo -a device=$())
else
  dev="none"
fi


jo time=$(date -u +%Y-%m-%d:%H:%M:%S)\
 uptime=$(uptime -p | awk '{print $2" "$3" "$4" "$5}')\
 distro=$(cat /etc/os-release | grep -Po 'PRETTY_NAME="\K[^"]+')\
 kernel=$(uname -r)\
 video=$(jo -a device=$(ls -ltrh /dev/video*))\
 mem=$(free -m | awk 'NR==2{printf "Memory Usage: %s/%sMB (%.2f%%)\n", $3,$2,$3*100/$2 }') #Memory Usage
