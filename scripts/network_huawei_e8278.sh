#!/bin/bash
#Monitor cellular connection with Huawei E8278
ENDPOINT=http://192.168.1.1

## not used yet
USERNAME=admin
PASSWORD=admin
BASE64_PASSWORD=$(printf $PASSWORD | base64)

LOGFILE=../tmp/network.out
JSONFILE=../tmp/network.json

### send a http request to a login and read the SMS inbox
#curl $ENDPOINT/api/user/login --data '<?xml version="1.0" encoding="UTF-8"?><request><Username>'$USERNAME'</Username><Password>'$BASE64_PASSWORD'</Password></request>' --compressed
#curl $ENDPOINT/api/sms/sms-list --data '<?xml version="1.0" encoding="UTF-8"?><request><PageIndex>1</PageIndex><ReadCount>20</ReadCount><BoxType>1</BoxType><SortType>0</SortType><Ascending>0</Ascending><UnreadPreferred>0</UnreadPreferred></request>'


## evaluate the network type
set_connection_type() {
Type="GPRS"
if ((2<=$1 && $1<=2))
then
		Type="GPRS"
elif ((3<=$1 && $1<=3))
then
		Type="EDGE"
elif ((4<=$1 && $1<=18))
then
		Type="3G"
elif ((19<=$1 && $1<=19))
then
		Type="4G LTE"
fi
}

curl -s $ENDPOINT/api/monitoring/status > $LOGFILE
curl -s $ENDPOINT/api/monitoring/traffic-statistics >> $LOGFILE
curl -s $ENDPOINT/api/net/current-plmn >> $LOGFILE
curl -s $ENDPOINT/api/monitoring/check-notifications >> $LOGFILE

## assing an array of element that will be sent to Darone.io
xml=( "unreadmessage" "currentnetworktype" "fullname" "currentconnecttime" "currentupload" "signalstrength" "signalicon" "currentdownload" "currentdownloadrate" "currentuploadrate" )

timestamp="$(date +"%s")"

## loop through the list of elements to create a key:value params
for i in "${xml[@]}"
do
		A=$(grep -i $i">" $LOGFILE | sed -e 's/<[^>]*>//g')
		eval $i=$A
done

## create a json
echo "{" > $JSONFILE

echo "\"timestamp\":\"$timestamp\"," >> $JSONFILE

for i in "${xml[@]}"
do
	A=$i
	## make sure to remove any character that is not number or alphabetical
	B=$(echo ${!i} | sed -e 's/[^0-9a-Z]//g')
	printf "\"%b\":\"%b\",\n" "$A" "$B" >> $JSONFILE
	## handle the CurrentNetworkType a bit different to map towards the function set_connection_type
	if [ "$A" = "CurrentNetworkType" ]; then
		set_connection_type $B
		echo "\"type\":\"$Type\"," >> $JSONFILE
	fi
done

echo "}" >> $JSONFILE

exit 0;
