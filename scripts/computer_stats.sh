#!/bin/bash
LOAD_LAST_MINUTE=$(uptime|awk '{print $8}')
CPUINFO_MODEL_NAME=$(cat /proc/cpuinfo|grep "model name"|awk '{print $4,$5,$6,$7}')
CPUINFO_MHZ=$(cat /proc/cpuinfo|grep "cpu MHz"|awk '{print $4}')
MEMINFO_TOTAL_KB=$(cat /proc/meminfo|grep "MemTotal:"|awk '{print $2}')
MEMINFO_FREE_KB=$(cat /proc/meminfo|grep "MemFree:"|awk '{print $2}')
DISKINFO_FREE=$(df / | tail -1|awk '{print $5}')
